## About the project

This is a demo project that consists of a REST API for a multi-user and multi-project work time tracking application.

## Getting started

Note that this project runs using docker compose. As such, if you don't have it installed, please follow the instructions on how to install Docker and Docker Compose at [docker docs](https://docs.docker.com/compose/install/).

1. Clone the repo:
    ```sh
    git clone https://gitlab.com/luis.guimaraes/demo-rest-api-project.git
    ``` 

2. Start up the containers
    ```sh
    docker compose up --build
    ```

3. The project will be enabled at `127.0.0.1:8000/` with all the available API endpoints

## Testing

To test, just use `python manage.py test` at the root directory

## Roadmap

- [ ] Improve authentication method to JWT.
- [ ] Improve testing tools, adding coverage.