from demoProjectBakerSoft import settings
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import TimeStampModel
from rest_framework.authtoken.models import Token

from .managers import UserManager


class User(AbstractUser, TimeStampModel):
    """ """

    objects = UserManager()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
