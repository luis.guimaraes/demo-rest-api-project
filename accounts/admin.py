from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import User


@admin.register(User)
class UserAdmin(ModelAdmin):
    """User admin."""

    search_fields = ["username"]
    list_display = ["id", "username", "email"]
