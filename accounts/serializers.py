from rest_framework.fields import CharField, EmailField
from rest_framework.serializers import ModelSerializer

from .models import User


class RegisterUserSerializer(ModelSerializer):
    """
    Login serializer with username and password
    """

    class Meta:
        model = User
        fields = ("username", "password", "email")

    def save(self, **kwargs):
        return User.objects.create_user(**self.validated_data)
