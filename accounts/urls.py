from django.urls import path

from rest_framework.authtoken.views import ObtainAuthToken

from .views import RegisterAPIView

app_name = "accounts"

urlpatterns = [
    path("login/", view=ObtainAuthToken.as_view(), name="login"),
    path("register/", view=RegisterAPIView.as_view(), name="register"),
]
