from django.shortcuts import get_object_or_404
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    GenericAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)

from projects.models import Project, UserProjectRole

from projects.serializers.projects import (
    BaseProjectSerializer,
    CreateProjectSerializer,
    DetailProjectSerializer,
    ProjectMemberSerializer,
    UpdateProjectSerializer,
)
from core.views import TokenAuthenticationMixin


class BaseProjectAPIView(TokenAuthenticationMixin, GenericAPIView):
    def get_queryset(self):
        return Project.objects.by_user(self.user)


class CreateProjectAPIView(BaseProjectAPIView, CreateAPIView):
    """
    API view for creating a new project
    """

    serializer_class = CreateProjectSerializer


class ListProjectAPIView(BaseProjectAPIView, ListAPIView):
    """
    API view for listing projects
    """

    serializer_class = BaseProjectSerializer


class GetProjectAPIView(BaseProjectAPIView, RetrieveAPIView):
    """
    API view for retrieving a single project
    """

    serializer_class = DetailProjectSerializer


class UpdateProjectAPIView(BaseProjectAPIView, UpdateAPIView):
    """
    API view for updating a single project name and description
    """

    serializer_class = UpdateProjectSerializer


class DeleteProjectAPIView(BaseProjectAPIView, DestroyAPIView):
    """
    API view for deleting a project
    """

    serializer_class = BaseProjectSerializer


class CreateProjectMemberAPIView(BaseProjectAPIView, CreateAPIView):
    """
    API view for creating a project member
    """

    serializer_class = ProjectMemberSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["project"] = self.get_object()
        return context


class DeleteProjectMemberAPIView(BaseProjectAPIView, DestroyAPIView):
    """
    API view for deleting a project member
    """

    serializer_class = ProjectMemberSerializer

    def get_object(self):
        project = super().get_object()
        serializer = self.get_serializer(data=self.request.POST)
        serializer.is_valid(raise_exception=True)
        user_project_role = get_object_or_404(
            UserProjectRole,
            project=project,
            user=serializer.validated_data.get("user"),
            role=serializer.validated_data.get("role"),
        )

        return user_project_role
