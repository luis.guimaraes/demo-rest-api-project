from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    GenericAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)
from projects.models import ProjectRole

from projects.serializers.project_roles import (
    BaseProjectRoleSerializer,
    UpdateProjectRoleSerializer,
)
from core.views import TokenAuthenticationMixin


class BaseProjectRoleAPIView(TokenAuthenticationMixin, GenericAPIView):
    queryset = ProjectRole.objects


class CreateProjectRoleAPIView(BaseProjectRoleAPIView, CreateAPIView):
    """
    API view for creating a new project role
    """

    serializer_class = BaseProjectRoleSerializer


class ListProjectRoleAPIView(BaseProjectRoleAPIView, ListAPIView):
    """
    API view for listing project roles
    """

    serializer_class = BaseProjectRoleSerializer


class GetProjectRoleAPIView(BaseProjectRoleAPIView, RetrieveAPIView):
    """
    API view for retrieving a single project role
    """

    serializer_class = BaseProjectRoleSerializer


class UpdateProjectRoleAPIView(BaseProjectRoleAPIView, UpdateAPIView):
    """
    API view for updating a single project role
    """

    serializer_class = UpdateProjectRoleSerializer


class DeleteProjectRoleAPIView(BaseProjectRoleAPIView, DestroyAPIView):
    """
    API view for deleting a single project role
    """

    serializer_class = BaseProjectRoleSerializer
