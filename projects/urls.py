from django.urls import path

from projects.views.projects import (
    CreateProjectAPIView,
    CreateProjectMemberAPIView,
    DeleteProjectAPIView,
    DeleteProjectMemberAPIView,
    GetProjectAPIView,
    ListProjectAPIView,
    UpdateProjectAPIView,
)
from projects.views.project_roles import (
    CreateProjectRoleAPIView,
    DeleteProjectRoleAPIView,
    GetProjectRoleAPIView,
    ListProjectRoleAPIView,
    UpdateProjectRoleAPIView,
)

app_name = "projects"

urlpatterns = [
    path("new/", view=CreateProjectAPIView.as_view(), name="new"),
    path("list/", view=ListProjectAPIView.as_view(), name="list"),
    path("<int:pk>/", view=GetProjectAPIView.as_view(), name="get"),
    path("<int:pk>/update/", view=UpdateProjectAPIView.as_view(), name="update"),
    path("<int:pk>/delete/", view=DeleteProjectAPIView.as_view(), name="delete"),
    path(
        "<int:pk>/member/new/",
        view=CreateProjectMemberAPIView.as_view(),
        name="new_member",
    ),
    path(
        "<int:pk>/member/delete/",
        view=DeleteProjectMemberAPIView.as_view(),
        name="delete_member",
    ),
    path("role/new/", view=CreateProjectRoleAPIView.as_view(), name="new_role"),
    path("role/list/", view=ListProjectRoleAPIView.as_view(), name="list_roles"),
    path("role/<int:pk>/", view=GetProjectRoleAPIView.as_view(), name="get_role"),
    path(
        "role/<int:pk>/update/",
        view=UpdateProjectRoleAPIView.as_view(),
        name="update_role",
    ),
    path(
        "role/<int:pk>/delete/",
        view=DeleteProjectRoleAPIView.as_view(),
        name="delete_role",
    ),
]
