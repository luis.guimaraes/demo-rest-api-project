from django.urls import reverse

from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from accounts.models import User
from projects.models import Project, ProjectRole


class ProjectTests(APITestCase):
    """
    Test suite for TimeLog views
    """

    def setUp(self) -> None:
        self.user = baker.make(User)
        self.headers = {"HTTP_AUTHORIZATION": f"Token {self.user.auth_token}"}

    def test_create_project(self):
        url = reverse("projects:new")
        data = {
            "name": "demo project",
            "description": "description",
        }
        response = self.client.post(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(Project.objects.count(), 1)
        self.assertEquals(Project.objects.get().name, "demo project")
        self.assertEquals(Project.objects.get().description, "description")
        self.assertEquals(Project.objects.get().created_by, self.user)

    def test_list_projects(self):
        project = baker.make(
            Project,
            name="demo project",
            description="description",
            created_by=self.user,
        )
        second_project = baker.make(
            Project,
            name="second demo project",
            description="second description",
            created_by=self.user,
        )
        url = reverse("projects:list")
        response = self.client.get(url, format="json", **self.headers)
        json_response = [
            {"pk": project.pk, "name": "demo project", "description": "description"},
            {
                "pk": second_project.pk,
                "name": "second demo project",
                "description": "second description",
            },
        ]
        self.assertEquals(response.json(), json_response)

    def test_update_project(self):
        project = baker.make(
            Project,
            name="demo project",
            description="description",
            created_by=self.user,
        )
        url = reverse("projects:update", kwargs={"pk": project.pk})
        data = {
            "name": "project demo",
            "description": "description test",
        }
        response = self.client.put(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(Project.objects.count(), 1)
        self.assertEquals(Project.objects.get().name, "project demo")
        self.assertEquals(Project.objects.get().description, "description test")
        self.assertEquals(Project.objects.get().created_by, self.user)

    def test_update_project_add_members(self):
        project = baker.make(
            Project,
            name="demo project",
            description="description",
            created_by=self.user,
        )
        user_johndoe = baker.make(User, username="johndoe")
        user_janedoe = baker.make(User, username="janedoe")
        baker.make(ProjectRole, name="developer", abbreviation="DEV")
        url = reverse("projects:update", kwargs={"pk": project.pk})
        data = {
            "members": [
                {"user": "johndoe", "role": "DEV"},
                {"user": "janedoe", "role": "DEV"},
            ],
        }
        response = self.client.put(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(Project.objects.count(), 1)
        self.assertEquals(project.members.count(), 2)
        self.assertEquals(user_johndoe.projects.get(), project)
        self.assertEquals(user_janedoe.projects.get(), project)

    def test_delete_project(self):
        project = baker.make(
            Project,
            name="demo project",
            description="description",
            created_by=self.user,
        )
        url = reverse("projects:delete", kwargs={"pk": project.pk})
        response = self.client.delete(url, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEquals(Project.objects.count(), 0)

    def test_create_project_member(self):
        project = baker.make(
            Project,
            name="demo project",
            description="description",
            created_by=self.user,
        )
        url = reverse("projects:new_member", kwargs={"pk": project.pk})
        user_johndoe = baker.make(User, username="johndoe")
        baker.make(ProjectRole, name="developer", abbreviation="DEV")
        data = {"user": "johndoe", "role": "DEV"}
        response = self.client.post(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(Project.objects.count(), 1)
        self.assertEquals(project.members.count(), 1)
        self.assertEquals(user_johndoe.projects.get(), project)
