from rest_framework import fields
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer


from projects.models import ProjectRole


class BaseProjectRoleSerializer(ModelSerializer):
    """
    Base Project serializer
    """

    class Meta:
        model = ProjectRole
        fields = ["pk", "name", "description", "abbreviation"]
        read_only_fields = ["pk"]

    def validate_abbreviation(self, value):
        if value in ProjectRole.objects.values_list("abbreviation", flat=True):
            raise ValidationError("Field is unique. Please assign a different value.")
        return value


class UpdateProjectRoleSerializer(BaseProjectRoleSerializer):
    """
    API view for updating project roles
    """

    name = fields.CharField(required=False)
    description = fields.CharField(required=False)
    abbreviation = fields.CharField(required=False)
