from rest_framework import fields
from rest_framework.serializers import ListSerializer, ModelSerializer, SlugRelatedField

from accounts.models import User
from projects.models import Project, ProjectRole, UserProjectRole


class BaseProjectSerializer(ModelSerializer):
    """
    Base Project serializer
    """

    class Meta:
        model = Project
        fields = ["pk", "name", "description"]
        read_only_fields = ["pk"]

    def __init__(self, instance=None, **kwargs):
        self.user = kwargs.get("context").get("user")
        super().__init__(instance, **kwargs)


class CreateProjectSerializer(BaseProjectSerializer):
    """
    Serializer for creating a new project
    """

    def create(self, validated_data):
        return super().create(
            validated_data={"created_by": self.user, **validated_data}
        )


class ProjectMemberSerializer(ModelSerializer):
    """
    Serializer for project members
    """

    user = SlugRelatedField(slug_field="username", queryset=User.objects)
    role = SlugRelatedField(slug_field="abbreviation", queryset=ProjectRole.objects)
    role_name = fields.CharField(source="role.name", read_only=True)

    class Meta:
        model = UserProjectRole
        fields = ["user", "role", "role_name"]

    def create(self, validated_data):
        validated_data["project"] = self.context.get("project")
        return super().create(validated_data)


class DetailProjectSerializer(BaseProjectSerializer):
    """
    Detail project serializer
    """

    members = ListSerializer(child=ProjectMemberSerializer())

    class Meta:
        model = Project
        fields = ["created", "created_by", "name", "description", "members"]

    def to_representation(self, instance):
        return {
            "created": instance.created,
            "created_by": instance.created_by.username,
            "name": instance.name,
            "description": instance.description,
            "members": instance.members_list,
        }


class UpdateProjectSerializer(BaseProjectSerializer):
    """
    Serializer for updating a project name, description, or add new members
    """

    members = ListSerializer(child=ProjectMemberSerializer(), required=False)
    name = fields.CharField(required=False)

    class Meta:
        model = Project
        fields = ["name", "description", "members"]

    def update(self, instance, validated_data):
        if "members" in validated_data:
            members = validated_data.pop("members")
            for member in members:
                if member not in instance.members.all():
                    _member_instance, _created = UserProjectRole.objects.get_or_create(
                        user=member.get("user"),
                        role=member.get("role"),
                        project=instance,
                    )
                    instance.members.add(member.get("user"))
        for field in validated_data:
            setattr(instance, field, validated_data[field])
        instance.save(update_fields=validated_data.keys())
        return instance

    def to_representation(self, instance):
        return {
            "created": instance.created,
            "created_by": instance.created_by.username,
            "name": instance.name,
            "description": instance.description,
            "members": instance.members_list,
        }
