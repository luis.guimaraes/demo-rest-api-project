from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Project, ProjectRole, UserProjectRole


@admin.register(Project)
class ProjectAdmin(ModelAdmin):
    pass


@admin.register(ProjectRole)
class ProjectRoleAdmin(ModelAdmin):
    pass


@admin.register(UserProjectRole)
class UserProjectRoleAdmin(ModelAdmin):
    list_filter = ("project", "role")
