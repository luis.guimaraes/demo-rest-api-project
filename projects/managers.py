from django.db.models import Manager


class ProjectRoleManager(Manager):
    """
    Manager for project roles
    """


class ProjectManager(Manager):
    """
    Manager for projects
    """

    def by_user(self, user):
        return self.filter(created_by=user)

    def user_projects(self, user):
        return self.filter(members__user=user)
