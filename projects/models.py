from functools import cached_property
from django.db import models

from core.models import TimeStampModel
from demoProjectBakerSoft import settings

from .managers import ProjectManager, ProjectRoleManager


class ProjectRole(TimeStampModel):
    """
    Model for Project Roles
    """

    name = models.CharField(name="name", help_text="Project role name", max_length=16)
    description = models.CharField(
        name="description",
        help_text=f"Project role description. Maximum of {settings.DESCRIPTION_LENGTH} characters.",
        max_length=settings.DESCRIPTION_LENGTH,
        blank=True,
    )
    abbreviation = models.CharField(
        name="abbreviation",
        help_text="Abbreviation of role for ease of assignment.",
        unique=True,
        max_length=4,
    )

    objects = ProjectRoleManager()

    def __str__(self) -> str:
        return f"{self.name} ({self.abbreviation})"


class Project(TimeStampModel):
    """
    Model for Project
    """

    created_by = models.ForeignKey(
        "accounts.User", name="created_by", on_delete=models.PROTECT
    )
    name = models.CharField(name="name", help_text="Project name", max_length=32)
    description = models.CharField(
        name="description",
        help_text=f"Project description. Maximum of {settings.DESCRIPTION_LENGTH} characters",
        max_length=settings.DESCRIPTION_LENGTH,
        blank=True,
    )
    members = models.ManyToManyField(
        to="accounts.User",
        through="UserProjectRole",
        name="members",
        help_text="Project members",
        related_name="projects",
    )

    objects = ProjectManager()

    @cached_property
    def members_list(self):
        return [
            {
                "user": user_project_role.user.username,
                "role": user_project_role.role.abbreviation,
                "role_name": user_project_role.role.name,
            }
            for user_project_role in (
                UserProjectRole.objects.prefetch_related(
                    "project", "role", "user"
                ).filter(project=self, user__in=self.members.all())
            )
        ]

    def __str__(self) -> str:
        return self.name


class UserProjectRole(TimeStampModel):
    user = models.ForeignKey("accounts.User", name="user", on_delete=models.CASCADE)
    role = models.ForeignKey(ProjectRole, name="role", on_delete=models.CASCADE)
    project = models.ForeignKey(Project, name="project", on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user", "role", "project"], name="unique user project role"
            )
        ]

    def __str__(self) -> str:
        return f"{self.user} - {self.role} - {self.project}"
