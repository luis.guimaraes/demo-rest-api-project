from datetime import datetime, timedelta
from django.urls import reverse

from model_bakery import baker
from rest_framework import status
from rest_framework.test import APITestCase

from accounts.models import User
from time_log.models import TimeLog
from projects.models import Project, ProjectRole, UserProjectRole


class TimeLogTests(APITestCase):
    """
    Test suite for TimeLog views
    """

    def setUp(self) -> None:
        self.project = baker.make(Project)
        self.user = baker.make(User)
        self.role = baker.make(ProjectRole)
        baker.make(
            UserProjectRole, user=self.user, role=self.role, project=self.project
        )
        self.headers = {"HTTP_AUTHORIZATION": f"Token {self.user.auth_token}"}

    def test_create_time_log(self):
        url = reverse("time_log:new")
        data = {
            "start_date": "2022-09-13 13:00:00",
            "end_date": "2022-09-13 19:00:00",
            "project": self.project.pk,
        }
        response = self.client.post(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        self.assertEquals(TimeLog.objects.count(), 1)
        self.assertEquals(TimeLog.objects.get().duration, timedelta(hours=6))

    def test_create_time_log_invalid_dates(self):
        url = reverse("time_log:new")
        data = {
            "start_date": "2022-09-13 19:00:00",
            "end_date": "2022-09-13 13:00:00",
            "project": self.project.pk,
        }
        response = self.client.post(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(TimeLog.objects.count(), 0)

    def test_update_time_log_start_date(self):
        time_log = baker.make(
            TimeLog,
            start_date=datetime(2022, 9, 13, 13, 0, 0),
            end_date=datetime(2022, 9, 13, 19, 0, 0),
            project=self.project,
            user=self.user,
        )
        url = reverse("time_log:update", kwargs={"pk": time_log.pk})
        data = {"start_date": "2022-09-13 16:00:00"}
        response = self.client.put(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(
            TimeLog.objects.get(pk=time_log.pk).duration, timedelta(hours=3)
        )

    def test_update_time_log_end_date(self):
        time_log = baker.make(
            TimeLog,
            start_date=datetime(2022, 9, 13, 13, 0, 0),
            end_date=datetime(2022, 9, 13, 19, 0, 0),
            project=self.project,
            user=self.user,
        )
        url = reverse("time_log:update", kwargs={"pk": time_log.pk})
        data = {"end_date": "2022-09-13 20:00:00"}
        response = self.client.put(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(
            TimeLog.objects.get(pk=time_log.pk).duration, timedelta(hours=7)
        )

    def test_update_time_log_invalid_dates(self):
        time_log = baker.make(
            TimeLog,
            start_date=datetime(2022, 9, 13, 13, 0, 0),
            end_date=datetime(2022, 9, 13, 19, 0, 0),
            project=self.project,
            user=self.user,
        )
        url = reverse("time_log:update", kwargs={"pk": time_log.pk})
        data = {"end_date": "2022-09-13 10:00:00"}
        response = self.client.put(url, data, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_time_log(self):
        time_log = baker.make(
            TimeLog,
            start_date=datetime(2022, 9, 13, 13, 0, 0),
            end_date=datetime(2022, 9, 13, 19, 0, 0),
            project=self.project,
            user=self.user,
        )
        url = reverse("time_log:delete", kwargs={"pk": time_log.pk})
        response = self.client.delete(url, format="json", **self.headers)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_create_project_time_log(self):
        second_user = baker.make(User)
        baker.make(
            UserProjectRole, user=second_user, project=self.project, role=self.role
        )
        url = reverse("time_log:project_create", kwargs={"project_id": self.project.pk})
        data = {
            "start_date": "2022-09-13 13:00:00",
            "end_date": "2022-09-13 19:00:00",
        }
        response = self.client.post(url, data, format="json", **self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(TimeLog.objects.count(), 2)
        self.assertEqual(self.user.time_log.count(), 1)
        self.assertEqual(second_user.time_log.count(), 1)
