from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    GenericAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
)

from core.views import TokenAuthenticationMixin
from projects.models import Project

from .models import TimeLog
from .serializers import (
    BaseProjectTimeLogSerializer,
    BaseTimeLogSerializer,
    CreateProjectTimeLogSerializer,
    CreateTimeLogSerializer,
    UpdateTimeLogSerializer,
)


class BaseTimeLogAPIView(TokenAuthenticationMixin, GenericAPIView):
    def get_queryset(self):
        return TimeLog.objects.by_user(self.user)


class CreateTimeLogAPIView(BaseTimeLogAPIView, CreateAPIView):
    """
    API view for creating a new time log
    """

    serializer_class = CreateTimeLogSerializer


class ListTimeLogAPIView(BaseTimeLogAPIView, ListAPIView):
    """
    API view for listing time logs
    """

    serializer_class = BaseTimeLogSerializer


class GetTimeLogAPIView(BaseTimeLogAPIView, RetrieveAPIView):
    """
    API view for retrieving a time log
    """

    serializer_class = BaseTimeLogSerializer


class UpdateTimeLogAPIView(BaseTimeLogAPIView, UpdateAPIView):
    """
    API view for updating a single time log
    """

    serializer_class = UpdateTimeLogSerializer


class DeleteTimeLogAPIView(BaseTimeLogAPIView, DestroyAPIView):
    """
    API view for deleting a single time log
    """

    serializer_class = BaseTimeLogSerializer


class BaseProjectTimeLogAPIView(BaseTimeLogAPIView):
    """
    Base class for handling time log requests concerning projects
    """

    lookup_field = "project_id"

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["project"] = Project.objects.get(pk=self.kwargs.get(self.lookup_field))
        return context


class CreateProjectTimeLogAPIView(BaseProjectTimeLogAPIView, CreateAPIView):
    """
    API view for creating a time log for all users in a project
    """

    serializer_class = CreateProjectTimeLogSerializer


class ListProjectTimeLogAPIView(BaseProjectTimeLogAPIView, ListAPIView):
    """
    API view for listing all time logs concerning a project
    """

    serializer_class = BaseProjectTimeLogSerializer

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related()
            .filter(project_id=self.kwargs.get(self.lookup_field))
        )
