from django.db.models import Manager


class TimeLogManager(Manager):
    """
    Manager for time logs
    """

    def by_user(self, user):
        return self.filter(user=user)
