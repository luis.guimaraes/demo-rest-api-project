from django.urls import path

from .views import (
    CreateProjectTimeLogAPIView,
    CreateTimeLogAPIView,
    DeleteTimeLogAPIView,
    GetTimeLogAPIView,
    ListProjectTimeLogAPIView,
    ListTimeLogAPIView,
    UpdateTimeLogAPIView,
)


app_name = "time_log"

urlpatterns = [
    path("new/", view=CreateTimeLogAPIView.as_view(), name="new"),
    path("list/", view=ListTimeLogAPIView.as_view(), name="list"),
    path("<int:pk>/", view=GetTimeLogAPIView.as_view(), name="get"),
    path("<int:pk>/update/", view=UpdateTimeLogAPIView.as_view(), name="update"),
    path("<int:pk>/delete/", view=DeleteTimeLogAPIView.as_view(), name="delete"),
    path(
        "project/<int:project_id>/new/",
        view=CreateProjectTimeLogAPIView.as_view(),
        name="project_create",
    ),
    path(
        "project/<int:project_id>/list/",
        view=ListProjectTimeLogAPIView.as_view(),
        name="project_list",
    ),
]
