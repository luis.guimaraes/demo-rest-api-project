from rest_framework import fields
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from .models import TimeLog


class BaseTimeLogSerializer(ModelSerializer):
    """
    Base time log serializer
    """

    class Meta:
        model = TimeLog
        fields = ["pk", "duration", "start_date", "end_date", "project", "description"]
        read_only_fields = ["pk", "duration"]

    def __init__(self, instance=None, **kwargs):
        self.user = kwargs.get("context").get("user")
        if "project" in self.fields:
            self.fields["project"].queryset = self.user.projects.all()
        super().__init__(instance, **kwargs)

    def _calculate_duration(self, start_date, end_date):
        return end_date - start_date

    def validate(self, attrs):
        if (attrs.get("end_date") or self.instance.end_date) <= (
            attrs.get("start_date") or self.instance.start_date
        ):
            raise ValidationError("'end_date' must be higher than 'start_date'.")
        return super().validate(attrs)


class CreateTimeLogSerializer(BaseTimeLogSerializer):
    """
    Serializer for creating a new time log
    """

    def _get_time_log_dict(self, user, validated_data):
        return {
            "user": user,
            "duration": self._calculate_duration(
                start_date=validated_data.get("start_date"),
                end_date=validated_data.get("end_date"),
            ),
            **validated_data,
        }

    def create(self, validated_data):
        return super().create(
            validated_data=self._get_time_log_dict(
                user=self.user, validated_data=validated_data
            )
        )


class UpdateTimeLogSerializer(BaseTimeLogSerializer):
    """
    Serializer for updating a time log
    """

    start_date = fields.DateTimeField(required=False)
    end_date = fields.DateTimeField(required=False)

    class Meta:
        model = TimeLog
        fields = ["pk", "duration", "start_date", "end_date", "project", "description"]
        read_only_fields = ["pk", "duration", "project"]

    def update(self, instance, validated_data):
        if "start_date" or "end_date" in validated_data:
            validated_data["duration"] = self._calculate_duration(
                start_date=validated_data.get("start_date", instance.start_date),
                end_date=validated_data.get("end_date", instance.end_date),
            )
        return super().update(instance=instance, validated_data=validated_data)


class BaseProjectTimeLogSerializer(BaseTimeLogSerializer):
    """
    Base serializer class for handling time logs concerning projects
    """

    class Meta:
        model = TimeLog
        fields = [
            "pk",
            "duration",
            "start_date",
            "end_date",
            "project",
            "description",
            "user",
        ]
        read_only_fields = ["pk", "duration", "project", "user"]

    def __init__(self, instance=None, **kwargs):
        self.project = kwargs.get("context").get("project")
        super().__init__(instance, **kwargs)


class CreateProjectTimeLogSerializer(
    BaseProjectTimeLogSerializer, CreateTimeLogSerializer
):
    """
    Serializer for creating a time log instance for all project users
    """

    def create(self, validated_data):
        validated_data["project"] = self.project
        """
        This approach is not perfect with the Django + SQLite combination, as bulk_create does not retrieve
        instance id's, however it is written here as the most efficient approach

        objs = []
        for user in self.project.members.all():
            objs.append(TimeLog(**self._get_time_log_dict(user=user, validated_data=validated_data)))
        TimeLog.objects.bulk_create(objs, batch_size=100)
        """
        objs = []
        for user in self.project.members.all():
            objs.append(
                super().create(
                    validated_data=self._get_time_log_dict(
                        user=user, validated_data=validated_data
                    )
                )
            )
        return objs

    def to_representation(self, objs):
        representation = []
        for instance in objs:
            representation.append(super().to_representation(instance))
        return {"time_logs": representation}
