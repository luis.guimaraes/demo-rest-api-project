from django.db import models

from core.models import TimeStampModel
from demoProjectBakerSoft import settings

from .managers import TimeLogManager


class TimeLog(TimeStampModel):
    """
    Model for time logging
    """

    start_date = models.DateTimeField(name="start_date", null=False)
    end_date = models.DateTimeField(name="end_date", null=False)
    duration = models.DurationField(name="duration", null=False)
    user = models.ForeignKey(
        "accounts.User", on_delete=models.PROTECT, related_name="time_log", null=False
    )
    project = models.ForeignKey(
        "projects.Project",
        on_delete=models.PROTECT,
        related_name="time_log",
        null=False,
    )
    description = models.CharField(
        name="description",
        help_text=f"Project role description. Maximum of {settings.DESCRIPTION_LENGTH} characters.",
        max_length=settings.DESCRIPTION_LENGTH,
        blank=True,
    )

    objects = TimeLogManager()
