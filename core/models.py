from django.db import models
from django.utils import timezone


class TimeStampModel(models.Model):
    """
    Model that registers creation and update dates to be reused by all models
    """

    created = models.DateTimeField(editable=False, default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        abstract = True
