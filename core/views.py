from django.http import JsonResponse

from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import GenericAPIView
from rest_framework.status import HTTP_403_FORBIDDEN


INVALID_TOKEN_MESSAGE = "Please provide a valid token"


class TokenAuthenticationMixin(TokenAuthentication, GenericAPIView):
    """
    Mixin to handle token authentication
    """

    def dispatch(self, request, *args, **kwargs):
        try:
            authenticated = self.authenticate(request)
        except exceptions.AuthenticationFailed:
            return JsonResponse(
                data={"error": INVALID_TOKEN_MESSAGE}, status=HTTP_403_FORBIDDEN
            )
        if not authenticated:
            return JsonResponse(
                data={"error": INVALID_TOKEN_MESSAGE}, status=HTTP_403_FORBIDDEN
            )
        self.user = authenticated[0]
        return super().dispatch(request, *args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["user"] = self.user
        return context
